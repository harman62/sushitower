//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    // Make a tower
    var sushiTower:[SKSpriteNode] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var chopSticks:[SKSpriteNode] = []
    
    //make variables to store current position
    var catPosition = "left"
    var chopstickPosition = "left"
     var chopstickPositions:[String] = []
    
    
    func spawnSushi() {
        // 1. Make a sushi
        let sushi = SKSpriteNode(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
        
        //Part2: add chopsticks
        //generate a random number
        //0 no stick
        //1 = stick on right
           //stick.position.x = sushi.position.x + 100
           //stick.position.y = sushi.position.y - 10
        //2 = stick on left
           //stick.position.x = sushi.position.x - 100
           //stick.position.y = sushi.position.y - 10
        
        //generate a number between 1 and 2
        let stickPosition = Int.random(in: 1..<3)
        if(stickPosition == 1){
             self.chopstickPositions.append("right")
            let chopStick = SKSpriteNode(imageNamed: "chopstick")
            chopStick.position.x = sushi.position.x + 100
            chopStick.position.y = sushi.position.y - 10
            addChild(chopStick)
             self.chopSticks.append(chopStick)
            //redraw stick facing other direction
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            chopStick.run(facingRight)
           
        }
        else if(stickPosition == 2){
             self.chopstickPositions.append("left")
            let chopStick = SKSpriteNode(imageNamed: "chopstick")
            chopStick.position.x = sushi.position.x - 100
            chopStick.position.y = sushi.position.y - 10
            addChild(chopStick)
             self.chopSticks.append(chopStick)
          
        }
        //add chopsticks
//        let chopStick = SKSpriteNode(imageNamed: "chopstick")
//        chopStick.position.x = sushi.position.x - 50
//        chopStick.position.y = sushi.position.y
//        addChild(chopStick)
        //add chopstick to array
       
    }
    
    
    
    
    
    
    override func didMove(to view: SKView) {
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        
        // build the tower
        self.buildTower()
    }
    
    func buildTower() {
        for _ in 0...5 {
            self.spawnSushi()
        }
        for i in 0...5 {
            print(self.chopstickPositions[0])
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        print(mousePosition)
        
        // When person taps mouse,
        // remove a piece from the tower & redraw the tower
        let pieceToRemove = self.sushiTower.first
        let stickToRemove = self.chopSticks.first
        if(stickToRemove != nil){
            stickToRemove?.removeFromParent()
            self.chopSticks.remove(at: 0)
            //loop through remaining sticks and redraw
            //redraw chopsticks
            //-----------------------
            for chopStick in self.chopSticks {
                chopStick.position.y = chopStick.position.y - SUSHI_PIECE_GAP
            }
        }
        if (pieceToRemove != nil) {
            // hide it from the screen
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            // STICK: Update stick positions array:
            self.chopstickPositions.remove(at:0)
            
            // loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            
            
            
            //make it an infinite tower
            //after removing and redrawing the tower, add a new piece
            self.spawnSushi()
        }
        
        
        // 1. detect where person clicked
        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
            self.catPosition = "left"
            
        }
        else {
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            cat.position = CGPoint(x:self.size.width*0.85, y:100)
            
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
            //save cats position
            self.catPosition = "right"
            
            
        }
        
        
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        
        //MARK: Win lose detection
        //----------------------
        //1. if cat and stick are on same side, you lose
        //2. if cat and stick are on diff side, you win
        
        let firstChopstick = self.chopstickPositions[0]
        if (catPosition == "left" && firstChopstick == "left") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "right") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "left" && firstChopstick == "right") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "left") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
        }
        
        
    }
    
}
